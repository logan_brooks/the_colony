﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailPellet : MonoBehaviour {
	private string trailType;
	public TrailPellet prevPellet;
	public TrailPellet PrevPellet{
		get{ return prevPellet; }
		set{ prevPellet = value; }

	}
	public TrailPellet nextPellet;
	public TrailPellet NextPellet{
		get{ return nextPellet; }
		set{ nextPellet = value; }
	}
	Renderer rend;

	public void init(string type, TrailPellet prev) {
		trailType = type;
		PrevPellet = prev;
		if(prevPellet != null)
			PrevPellet.NextPellet = this;
		Debug.Log(trailType);
	}

	void Start() {
		rend = GetComponent<Renderer> ();
		Debug.Log("START");
		Debug.Log(trailType);
		if(trailType == "EXPLORATION") {
			rend.material.color = Color.blue;
		}
		else {
			rend.material.color = Color.red;
		}
		
	}
}
