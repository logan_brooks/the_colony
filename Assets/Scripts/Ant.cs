﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Ant : MonoBehaviour {

	public enum State {MOVING, SENSING};
	public GameObject hTrail;
	public int seekTime = 20;
	public float seekTimer = 0;
	public int senseTime = 2;
	private float radius = 100;

	public State state;
	private IAstarAI ai;
	public Vector3 target;
	
	public Trail myTrail;
	public Trail followingTrail;
	// Use this for initialization
	void Start () {
		// state = gameObject.GetComponent<StateMachine>();
		ai = GetComponent<IAstarAI>();
		state = State.SENSING;	
		// myTrail = gameObject.AddComponent<Trail>();
		// myTrail.init(this, "Exploration");
		myTrail = transform.parent.GetComponentInChildren<Trail>();
		myTrail.init(this, "Exploration");
		// hTrail = new GameObject();
		// hTrail.transform.SetParent(transform.parent);
		
	}
	
	
	// Update is called once per frame
	void Update () {
		if(state == State.SENSING) {
			seekTimer += Time.deltaTime;
			// Debug.Log(seekTimer);
			if(seekTimer > senseTime) {
				seekTimer = 0;
				
				state = State.MOVING;
				myTrail.TurnOn();
			}
		}
		else {
			if (!ai.pathPending && (ai.reachedEndOfPath || !ai.hasPath)) {
				Debug.Log("Moving...");
				target = PickRandomPoint();
				ai.destination = target;
				ai.SearchPath();
        	}
			else if(ai.reachedEndOfPath){
				state = State.SENSING;
				myTrail.TurnOff();
			}
			
		}
	}

	private void Sense() {
		
	}

	Vector3 PickRandomPoint () {
        Vector3 point = Random.insideUnitSphere * radius;

        point.y = 0;
        point += ai.position;
         Debug.Log(point);
        return point;
    }
}
