﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trail : MonoBehaviour {

	public enum TrailType {EXPLORATION, FORAGING};
	public TrailType trailType;
	public TrailPellet pelletPrefab;
	private Ant owner;
	private TrailPellet[] pellets;
	public TrailPellet lastPellet;

	private float pelletEmissionTimer = 0;
	private float pelletEmissionRate = 1.5f;

	public bool emitterActive = false;
	
	public void init(Ant o, string type) {
		owner = o;
		// trailType = type;
		// pelletPrefab = (TrailPellet) Resources.Load("Prefabs/TrailPellet");
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(emitterActive) {
			pelletEmissionTimer += Time.deltaTime;
			
			if(pelletEmissionTimer > pelletEmissionRate) {
				pelletEmissionTimer = 0;
				Debug.Log("CREATE PELLET");
				TrailPellet pellet = Instantiate(pelletPrefab, owner.transform.position, Quaternion.identity);
				pellet.transform.parent = transform;
				pellet.init(trailType.ToString(), lastPellet);
				lastPellet = pellet;
			}
		}
	}

	public void TurnOff() {
		emitterActive = false;
	}

	public void TurnOn() {
		emitterActive = true;
	}
}
